/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.libraryproject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author robb
 */
public class DataBaseCommunicatorTest {

    public DataBaseCommunicatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of listUsers method, of class DataBaseCommunicator.
     */
    @Test
    public void testListUsers() {
        System.out.println("listUsers");
        DataBaseCommunicator instance = new DataBaseCommunicator();
        int expResult = 2000;
        int result = instance.listUsers().size();
        assertTrue(expResult < result);
    }

    /**
     * Test of listBooks method, of class DataBaseCommunicator.
     */
    @Test
    public void testListBooks() {
        System.out.println("listBooks");
        DataBaseCommunicator instance = new DataBaseCommunicator();
        int expResult = 999;
        int result = instance.listBooks().size();
        assertTrue(expResult < result);
    }

    /**
     * Test of addNewUser method, of class DataBaseCommunicator.
     */
    @Test
    public void testAddNewUser() {
        System.out.println("addNewUser");
        int r = (int)(Math.random()*10000000);
        User user = new User(r+"Frank", r+"Robi", r+"Budapest 1071", r+"emailcím", new Date(2005,5,13));
        DataBaseCommunicator instance = new DataBaseCommunicator();
        int userId = instance.addNewUser(user);
        assertNotEquals(userId, 0);
        User newUser= instance.getUserById(userId);
        String newName = r+"Robi";
        String expResult = newUser.getLast_name();
        assertEquals(expResult, newName);
        
    }

    /**
     * Test of getUserById method, of class DataBaseCommunicator.
     */
    @Test
    public void testGetUserById() {
        System.out.println("getUserById");
        int id = 900;
        DataBaseCommunicator instance = new DataBaseCommunicator();
        User result = instance.getUserById(id);
        String firstName = result.getFirst_name();
        assertEquals(firstName, "Alexandra");
        String lastName = result.getLast_name();
        assertEquals(lastName, "Okeshott");
        String address = result.getAddress();
        assertEquals(address, "30 Milwaukee Way");     
    }

    /**
     * Test of getBookById method, of class DataBaseCommunicator.
     */
    @Test
    public void testGetBookById() {
        System.out.println("getBookById");
        int id = 0;
        DataBaseCommunicator instance = new DataBaseCommunicator();
        Book expResult = null;
        Book result = instance.getBookById(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of searchBooksByTitle method, of class DataBaseCommunicator.
     */
    @Test
    public void testSearchBooksByTitle() {
        System.out.println("searchBooksByTitle");
        String titleForSearch = "co";
        DataBaseCommunicator instance = new DataBaseCommunicator();
        ArrayList<Book> expResult = null;
        ArrayList<Book> result = instance.searchBooksByTitle(titleForSearch);
        assertEquals(62, result.size());
    }

    /**
     * Test of searchBooksByAuthor method, of class DataBaseCommunicator.
     */
    @Test
    public void testSearchBooksByAuthor() {
        System.out.println("searchBooksByAuthor");
        String authorNameForSearch = "co";
        DataBaseCommunicator instance = new DataBaseCommunicator();
        ArrayList<Book> expResult = null;
        ArrayList<Book> result = instance.searchBooksByAuthor(authorNameForSearch);
        assertEquals(69, result.size());
    }

    /**
     * Test of getBookIdByTitle method, of class DataBaseCommunicator.
     */
    @Test
    public void testGetBookIdByTitle() {
        System.out.println("getBookIdByTitle");
        String title = "";
        DataBaseCommunicator instance = new DataBaseCommunicator();
        ArrayList<Integer> expResult = null;
        ArrayList<Integer> result = instance.getBookIdByTitle(title);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBookIdByAuthor method, of class DataBaseCommunicator.
     */
    @Test
    public void testGetBookIdByAuthor() {
        System.out.println("getBookIdByAuthor");
        String author = "";
        DataBaseCommunicator instance = new DataBaseCommunicator();
        ArrayList<Integer> expResult = null;
        ArrayList<Integer> result = instance.getBookIdByAuthor(author);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getBookIdByTitleAndAuthor method, of class DataBaseCommunicator.
     */
    @Test
    public void testGetBookIdByTitleAndAuthor() {
        System.out.println("getBookIdByTitleAndAuthor");
        String title = "";
        String author = "";
        DataBaseCommunicator instance = new DataBaseCommunicator();
        int expResult = 0;
        int result = instance.getBookIdByTitleAndAuthor(title, author);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addNewBook method, of class DataBaseCommunicator.
     */
    @Test
    public void testAddNewBook() {
        System.out.println("addNewBook");
        Book book = null;
        DataBaseCommunicator instance = new DataBaseCommunicator();
        int expResult = 0;
        int result = instance.addNewBook(book);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAvailableCopiesCount method, of class DataBaseCommunicator.
     */
    @Test
    public void testGetAvailableCopiesCount_int() {
        System.out.println("getAvailableCopiesCount");
        int bookId = 0;
        DataBaseCommunicator instance = new DataBaseCommunicator();
        int expResult = 0;
        int result = instance.getAvailableCopiesCount(bookId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAvailableCopiesCount method, of class DataBaseCommunicator.
     */
    @Test
    public void testGetAvailableCopiesCount_String_String() {
        System.out.println("getAvailableCopiesCount");
        String title = "";
        String author = "";
        DataBaseCommunicator instance = new DataBaseCommunicator();
        int expResult = 0;
        int result = instance.getAvailableCopiesCount(title, author);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAvailableCopiesCount method, of class DataBaseCommunicator.
     */
    @Test
    public void testGetAvailableCopiesCount_Book() {
        System.out.println("getAvailableCopiesCount");
        Book book = null;
        DataBaseCommunicator instance = new DataBaseCommunicator();
        int expResult = 0;
        int result = instance.getAvailableCopiesCount(book);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of rentBook method, of class DataBaseCommunicator.
     */
    @Test
    public void testRentBook() {
        System.out.println("rentBook");
        int bookId = 0;
        DataBaseCommunicator instance = new DataBaseCommunicator();
        instance.rentBook(bookId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testSearchUser() {
        System.out.println("searchUser");
        DataBaseCommunicator instance = new DataBaseCommunicator();
        ArrayList<User> resList = instance.searchUser("aul", "ro", "");
        assertEquals(4, resList.size());
        
    }

}
