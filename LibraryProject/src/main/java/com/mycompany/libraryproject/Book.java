/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.libraryproject;


/**
 *
 * @author robb
 */
public class Book {
    
    int bookId;
    String title;
    int year;
    String isbn;
    String authorName;

    public Book(int bookId, String title, int year, String isbn, String authorName) {
        this.bookId = bookId;
        this.title = title;
        this.year = year;
        this.isbn = isbn;
        this.authorName = authorName;
    }
    
    public Book(String title, int year, String isbn, String authorName) {
        this.bookId = bookId;
        this.title = title;
        this.year = year;
        this.isbn = isbn;
        this.authorName = authorName;
    }
    
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    
    
    public String printBook(){
        
        return authorName+": "+title+", year of release:"+year+", ISBN:"+isbn;
        
    }
    
    
}
