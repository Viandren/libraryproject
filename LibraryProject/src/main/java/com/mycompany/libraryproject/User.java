/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.libraryproject;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author robb
 */
public class User {

    int userId;
    String first_name;
    String last_name;
    String address;
    String email;
    Date memberSince;
    ArrayList<Book> rentedBooks;
    ArrayList<Rental> rentals;

    public User(int userId, String first_name, String last_name, String address, String email, Date memberSince) {
        this.userId = userId;
        this.first_name = first_name;
        this.last_name = last_name;
        this.address = address;
        this.email = email;
        this.memberSince = memberSince;
    }

    public User(String first_name, String last_name, String address, String email, Date memberSince) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.address = address;
        this.email = email;
        this.memberSince = memberSince;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Book> getRentedBooks() {
        return rentedBooks;
    }

    public void setRentedBooks(ArrayList<Book> rentedBooks) {
        this.rentedBooks = rentedBooks;
    }

    public ArrayList<Rental> getRentals() {
        return rentals;
    }

    public void setRentals(ArrayList<Rental> rentals) {
        this.rentals = rentals;
    }

    public Date getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(Date memberSince) {
        this.memberSince = memberSince;
    }

    public String printUser() {
        return first_name + ", " + last_name + " - e mail: " + email + ", address: " + address + ", member since: " + memberSince;
    }

}
