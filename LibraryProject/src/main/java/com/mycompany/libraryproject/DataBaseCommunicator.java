/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.libraryproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author robb
 */
public class DataBaseCommunicator {

    String url;
    Connection conn;
    String sql;

    public DataBaseCommunicator() {

        url = "jdbc:mysql://localhost:3306/ady_library";
        try {
            conn = DriverManager.getConnection(url, "robi", "bubuka");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        sql = "";
    }

    public ArrayList<User> listUsers() {
        ArrayList resList = new ArrayList<>();

        sql = "SELECT * FROM user";
        try {
            PreparedStatement getUsersPs = conn.prepareStatement(sql);
            ResultSet usersResult = getUsersPs.executeQuery();

            while (usersResult.next()) {
                int userId = usersResult.getInt(1);
                String first_name = usersResult.getString(2);
                String last_name = usersResult.getString(3);
                String address = usersResult.getString(4);
                String email = usersResult.getString(5);
                Date memberSince = usersResult.getDate(6);
                resList.add(new User(userId, first_name, last_name, address, email, memberSince));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resList;
    }

    public ArrayList<Book> listBooks() {
        ArrayList resList = new ArrayList<>();

        sql = "SELECT * FROM book";
        try {
            PreparedStatement getBooksPs = conn.prepareStatement(sql);
            ResultSet booksResult = getBooksPs.executeQuery();

            while (booksResult.next()) {
                int bookId = booksResult.getInt(1);
                String title = booksResult.getString(2);
                int year = booksResult.getInt(3);
                String isbn = booksResult.getString(4);
                String authorName = booksResult.getString(5);

                resList.add(new Book(bookId, title, year, isbn, authorName));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resList;
    }

    public int addNewUser(User user) {
        sql = "INSERT INTO user VALUES (NULL, ?, ?, ?, ?, NOW())";
        String first_name = user.getFirst_name();
        String last_name = user.getLast_name();
        String address = user.getAddress();
        String email = user.getEmail();

        int resId = 0;
        try {
            PreparedStatement newUserPs = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            newUserPs.setString(1, first_name);
            newUserPs.setString(2, last_name);
            newUserPs.setString(3, address);
            newUserPs.setString(4, email);

            newUserPs.executeUpdate();
            ResultSet newUserRs = newUserPs.getGeneratedKeys();
            newUserRs.next();
            resId = newUserRs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resId;
    }

    public User getUserById(int id) {
        User resUser = null;
        sql = "SELECT * FROM user WHERE user_id=?";
        try {
            PreparedStatement getUsersPs = conn.prepareStatement(sql);
            getUsersPs.setInt(1, id);
            ResultSet usersResult = getUsersPs.executeQuery();

            while (usersResult.next()) {
                int userId = usersResult.getInt(1);
                String first_name = usersResult.getString(2);
                String last_name = usersResult.getString(3);
                String address = usersResult.getString(4);
                String email = usersResult.getString(5);
                Date memberSince = usersResult.getDate(6);
                resUser = new User(userId, first_name, last_name, address, email, memberSince);

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resUser;
    }

    public Book getBookById(int id) {
        Book resBook = null;
        sql = "SELECT * FROM book WHERE book_id=?";
        try {
            PreparedStatement getBookPs = conn.prepareStatement(sql);
            getBookPs.setInt(1, id);
            ResultSet bookResult = getBookPs.executeQuery();

            while (bookResult.next()) {
                int bookId = bookResult.getInt(1);
                String title = bookResult.getString(2);
                int year = bookResult.getInt(3);
                String isbn = bookResult.getString(4);
                String authorName = bookResult.getString(5);
                resBook = new Book(bookId, title, year, isbn, authorName);

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resBook;
    }

    public ArrayList<Book> searchBooksByTitle(String titleForSearch) {
        ArrayList<Book> resList = new ArrayList<>();
        sql = "SELECT * FROM book WHERE title LIKE '%"+titleForSearch+"%'";
        try {
            PreparedStatement searchBooksPs = conn.prepareStatement(sql);
            ResultSet searchBooksRs = searchBooksPs.executeQuery();
            while (searchBooksRs.next()) {
                int bookId = searchBooksRs.getInt(1);
                String title = searchBooksRs.getString(2);
                int year_released = searchBooksRs.getInt(3);
                String ISBN = searchBooksRs.getString(4);
                String authorName = searchBooksRs.getString(5);

                resList.add(new Book(bookId, title, year_released, ISBN, authorName));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return resList;
    }

    public ArrayList<Book> searchBooksByAuthor(String authorNameForSearch) {
        ArrayList<Book> resList = new ArrayList<>();
        sql = "SELECT * FROM book WHERE author LIKE '%"+authorNameForSearch+"%'";
        try {
            PreparedStatement searchBooksPs = conn.prepareStatement(sql);
            ResultSet searchBooksRs = searchBooksPs.executeQuery();
            while (searchBooksRs.next()) {
                int bookId = searchBooksRs.getInt(1);
                String title = searchBooksRs.getString(2);
                int year_released = searchBooksRs.getInt(3);
                String ISBN = searchBooksRs.getString(4);
                String authorName = searchBooksRs.getString(5);

                resList.add(new Book(bookId, title, year_released, ISBN, authorName));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return resList;
    }

    public ArrayList<Integer> getBookIdByTitle(String title) {
        ArrayList<Integer> bookIdList = new ArrayList<>();
        sql = "SELECT book_id FROM book WHERE title LIKE '%?%'";
        try {
            PreparedStatement searchBooksPs = conn.prepareStatement(sql);
            searchBooksPs.setString(1, title);
            ResultSet searchBooksRs = searchBooksPs.executeQuery();
            while (searchBooksRs.next()) {
                bookIdList.add(searchBooksRs.getInt(1));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return bookIdList;
    }

    public ArrayList<Integer> getBookIdByAuthor(String author) {
        ArrayList<Integer> bookIdList = new ArrayList<>();
        sql = "SELECT * FROM book WHERE author LIKE '%?%'";
        try {
            PreparedStatement searchBooksPs = conn.prepareStatement(sql);
            searchBooksPs.setString(1, author);
            ResultSet searchBooksRs = searchBooksPs.executeQuery();
            while (searchBooksRs.next()) {
                bookIdList.add(searchBooksRs.getInt(1));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return bookIdList;
    }
    
    public int getBookIdByTitleAndAuthor(String title, String author) {
        int bookId = 0;
        sql = "SELECT book_id FROM book WHERE title=? AND author=?";
        try {
            PreparedStatement searchBooksPs = conn.prepareStatement(sql);
            searchBooksPs.setString(1, title);
            searchBooksPs.setString(2, author);
            ResultSet searchBooksRs = searchBooksPs.executeQuery();
            while (searchBooksRs.next()) {
                bookId = searchBooksRs.getInt(1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return bookId;
    }

    public int addNewBook(Book book) {
        sql = "INSERT INTO book VALUES (NULL, ?, ?, ?, ?)";
        String title = book.getTitle();
        int year = book.getYear();
        String isbn = book.getIsbn();
        String author = book.getAuthorName();

        int resId = 0;
        try {
            PreparedStatement newUserPs = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            newUserPs.setString(1, title);
            newUserPs.setInt(2, year);
            newUserPs.setString(3, isbn);
            newUserPs.setString(4, author);

            newUserPs.executeUpdate();
            ResultSet newUserRs = newUserPs.getGeneratedKeys();
            newUserRs.next();
            resId = newUserRs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return resId;
    }

    public int getAvailableCopiesCount(int bookId) {
        sql = "SELECT COUNT(*) FROM inventory VALUES WHERE book_id=?";
        int cnt = 0;
        try {
            PreparedStatement bookCountPs = conn.prepareStatement(sql);
            bookCountPs.setInt(1, bookId);

            bookCountPs.executeQuery();
            ResultSet bookCountRs = bookCountPs.getGeneratedKeys();
            if (bookCountRs.next()) {
                cnt = bookCountRs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return cnt;
    }
    
    public int getAvailableCopiesCount(String title, String author) {
        sql = "SELECT COUNT(*) FROM inventory VALUES WHERE book_id=?";
        int cnt = 0;
        try {
            PreparedStatement bookCountPs = conn.prepareStatement(sql);
            bookCountPs.setInt(1, getBookIdByTitleAndAuthor(title, author));

            bookCountPs.executeQuery();
            ResultSet bookCountRs = bookCountPs.getGeneratedKeys();
            if (bookCountRs.next()) {
                cnt = bookCountRs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return cnt;
    }
    
    public int getAvailableCopiesCount(Book book) {
        sql = "SELECT COUNT(*) FROM inventory VALUES WHERE book_id=?";
        int cnt = 0;
        try {
            PreparedStatement bookCountPs = conn.prepareStatement(sql);
            bookCountPs.setInt(1, getBookIdByTitleAndAuthor(book.getTitle(), book.getAuthorName()));

            bookCountPs.executeQuery();
            ResultSet bookCountRs = bookCountPs.getGeneratedKeys();
            if (bookCountRs.next()) {
                cnt = bookCountRs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return cnt;
    }
    
    public void rentBook(int bookId){
        
        if (getAvailableCopiesCount(bookId)>0){
            
            
            
        }
        
    }
    
    
    public ArrayList<User> searchUser(String first_name, String last_name, String email){
        ArrayList<User> userList = new ArrayList<>();
        sql = "SELECT * FROM user WHERE first_name LIKE '%"+first_name+"%' AND last_name LIKE '%"+last_name+"%' AND email LIKE '%"+email+"%'";
        try {
            PreparedStatement searchUsersPs = conn.prepareStatement(sql);
            ResultSet searchUsersRs = searchUsersPs.executeQuery();
            while (searchUsersRs.next()) {
                int userId = searchUsersRs.getInt(1);
                String fname = searchUsersRs.getString(2);
                String lname = searchUsersRs.getString(3);
                String address = searchUsersRs.getString(4);
                String em = searchUsersRs.getString(5);
                Date memberSince = searchUsersRs.getDate(6);

                userList.add(new User(userId, fname, lname, address, em, memberSince));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return userList;
    }
    
    
    

}
